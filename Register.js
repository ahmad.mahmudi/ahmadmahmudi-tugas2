import React from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import signin from './signin';

const FormRegister = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.imagebox}>
        <Image style={styles.image} source={require('./gambar.png')}></Image>
      </View>
      <View style={styles.textbox}>
        <Text style={styles.text}>Create an account</Text>
      </View>
      <View style={styles.inputbox}>
        <TextInput style={styles.input} placeholder="Name"></TextInput>
        <TextInput style={styles.input} placeholder="Email"></TextInput>
        <TextInput
          keyboardType="numeric"
          style={styles.input}
          placeholder="Phone"></TextInput>
        <TextInput
          secureTextEntry={true}
          style={styles.input}
          placeholder="Password"></TextInput>
      </View>
      <View style={styles.signbox}>
        <TouchableOpacity style={styles.signbutton}>
          <Text style={styles.signtext}>Sign Up</Text>
        </TouchableOpacity>
      </View>
      <View style={[styles.footerbox, styles.shadowprop]}>
        <Text style={styles.text1}>
          Already have account?
          <Text
            onPress={() => navigation.navigate('signin')}
            style={styles.text2}>
            {' '}
            Login
          </Text>
        </Text>
      </View>
    </View>
  );
};

const Stack = createNativeStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Register" component={FormRegister}></Stack.Screen>
      <Stack.Screen name="signin" component={signin}></Stack.Screen>
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#f4f4f4',
  },

  imagebox: {
    alignSelf: 'center',
  },

  image: {
    width: 200,
  },

  textbox: {
    alignSelf: 'center',
    marginTop: 20,
    marginBottom: 30,
  },

  text: {
    fontFamily: 'Nunito',
    fontSize: 20,
  },

  inputbox: {},

  input: {
    borderWidth: 0,
    marginTop: 10,
    marginBottom: 10,
    width: 350,
    height: 45,
    alignSelf: 'center',
    backgroundColor: 'white',
    borderRadius: 5,
    paddingLeft: 20,
  },

  signbox: {
    alignSelf: 'center',
    marginTop: 10,
  },

  signbutton: {
    backgroundColor: '#005195',
    width: 350,
    height: 50,
    borderRadius: 5,
    justifyContent: 'center',
  },

  signtext: {
    color: 'white',
    alignSelf: 'center',
  },

  footerbox: {
    display: 'flex',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    width: '100%',
    position: 'absolute',
    bottom: 0,
    height: '5%',
  },

  shadowprop: {
    shadowOffset: {width: 0, height: 12},
    shadowColor: '#000000',
    shadowOpacity: 1,
    shadowRadius: 16,
    elevation: 24,
  },

  text1: {},

  text2: {
    fontWeight: 'bold',
  },
});
export default MyStack;
