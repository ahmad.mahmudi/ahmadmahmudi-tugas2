import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import signin from './signin';

function Forgot({navigation}) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.backbtn}
        onPress={() => navigation.goBack()}>
        <Image source={require('./back-button.png')}></Image>
      </TouchableOpacity>
      <Image style={styles.logoapp} source={require('./gambar.png')}></Image>
      <Text style={styles.text1}> Reset your password </Text>
      <TextInput style={styles.input} placeholder="Email"></TextInput>
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('signin')}>
        <Text style={styles.btntext}>Request Reset</Text>
      </TouchableOpacity>
    </View>
  );
}

const Stack = createNativeStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Forgot" component={Forgot}></Stack.Screen>
      <Stack.Screen name="signin" component={signin}></Stack.Screen>
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f4f4f4',
    justifyContent: 'center',
  },
  backbtn: {
    position: 'absolute',
    top: 45,
    left: 13,
  },
  logoapp: {
    width: 190,
    height: 45,
    alignSelf: 'center',
    marginBottom: 25,
  },
  text1: {
    fontSize: 20,
    textAlign: 'center',
    marginBottom: 10,
    marginTop: 10,
  },
  input: {
    backgroundColor: 'white',
    width: 350,
    alignSelf: 'center',
    paddingLeft: 15,
    borderRadius: 5,
    marginBottom: 5,
    marginTop: 10,
  },
  button: {
    backgroundColor: '#2e3283',
    paddingTop: 15,
    paddingBottom: 15,
    width: 350,
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginTop: 15,
  },
  btntext: {
    color: 'white',
  },
});
export default MyStack;
