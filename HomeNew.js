import {
  View,
  Text,
  Button,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import signin from './signin';

function Menu({navigation}) {
  return (
    <View style={styles.container}>
      <Image style={styles.banner} source={require('./banner.png')}></Image>
      <View style={styles.menuicon}>
        <Image source={require('./gambar1.png')}></Image>
        <Image source={require('./gambar2.png')}></Image>
        <Image source={require('./gambar3.png')}></Image>
        <Image source={require('./gambar4.png')}></Image>
      </View>
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('signin')}>
        <Text style={styles.buttontext}>More ...</Text>
      </TouchableOpacity>
    </View>
  );
}
function MyBooking() {
  return (
    <View>
      <Text>My Booking</Text>
    </View>
  );
}
function Help() {
  return (
    <View>
      <Text>Help</Text>
    </View>
  );
}
function Profile({navigation}) {
  return (
    <View>
      <TouchableOpacity
        style={styles.buttonprofile}
        onPress={() => navigation.navigate('signin')}>
        <Text style={styles.profiletext}>Sign In</Text>
      </TouchableOpacity>
    </View>
  );
}

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

function MyTabs() {
  return (
    <Tab.Navigator screenOptions={{headerShown: false}}>
      <Tab.Screen
        name="Home"
        component={Menu}
        options={{
          tabBarIcon: () => (
            <Image style={styles.logo} source={require('./Home.png')}></Image>
          ),
        }}
      />
      <Tab.Screen
        name="My Booking"
        component={MyBooking}
        options={{
          tabBarIcon: () => (
            <Image
              style={styles.logo}
              source={require('./MyBooking.png')}></Image>
          ),
        }}
      />
      <Tab.Screen
        name="Help"
        component={Help}
        options={{
          tabBarIcon: () => (
            <Image style={styles.logo} source={require('./Help.png')}></Image>
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: () => (
            <Image
              style={styles.logo}
              source={require('./Profile.png')}></Image>
          ),
        }}
      />
    </Tab.Navigator>
  );
}

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Tab" component={MyTabs} />
        <Stack.Screen name="Home" component={Menu} />
        <Stack.Screen name="signin" component={signin} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  banner: {
    width: '100%',
    height: 155,
  },
  menuicon: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 20,
  },
  button: {
    backgroundColor: '#2E3283',
    width: '30%',
    borderRadius: 10,
    alignSelf: 'center',
    position: 'absolute',
    bottom: 15,
  },
  buttontext: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 18,
    padding: 10,
  },
  logo: {
    width: 30,
    height: 30,
  },
  signin: {
    backgroundColor: '#2E3283',
    width: '30%',
    borderRadius: 10,
    alignSelf: 'center',
    marginTop: '50%',
    marginBottom: '50%',
  },
  signintext: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 18,
    padding: 10,
  },
  buttonprofile: {
    width: '30%',
    backgroundColor: 'blue',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 10,
    marginTop: '50%',
    paddingBottom: 10,
    paddingTop: 10,
  },
  profiletext: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 18,
  },
});
export default App;
