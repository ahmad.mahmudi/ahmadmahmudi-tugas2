import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Register from './Register';
import Forgot from './Forgot';

function SignIn({navigation}) {
  return (
    <View style={styles.container}>
      <View style={styles.logoappbox}>
        <Image source={require('./gambar.png')} style={styles.logoapp}></Image>
      </View>
      <Text style={styles.signintext}>Please sign in to continue</Text>
      <View style={styles.inputbox}>
        <TextInput placeholder="Username" style={styles.input}></TextInput>
        <TextInput
          placeholder="Password"
          style={styles.input}
          secureTextEntry={true}></TextInput>
      </View>
      <View style={styles.buttonbox}>
        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttontext}>Sign In</Text>
        </TouchableOpacity>
      </View>
      <Text
        onPress={() => navigation.navigate('Forgot')}
        style={styles.forpasstext}>
        Forgot Password
      </Text>
      <View style={styles.loginbox}>
        <View style={styles.garis}></View>
        <Text style={styles.logintext}>Login with</Text>
        <View style={styles.garis}></View>
      </View>
      <View style={styles.imagebox}>
        <Image source={require('./logofb.png')} style={styles.logofb}></Image>
        <Image
          source={require('./logogoogle.png')}
          style={styles.logogoogle}></Image>
      </View>
      <View style={styles.versionbox}>
        <Text style={styles.versiontext1}>App Version</Text>
        <Text style={styles.versiontext2}>2.8.3</Text>
      </View>
      <View style={styles.footerbox}>
        <Text style={styles.footertext1}>
          Don't have account?
          <TouchableOpacity onPress={() => navigation.navigate('Register')}>
            <Text style={styles.footertext2}> Sign Up</Text>
          </TouchableOpacity>
        </Text>
      </View>
    </View>
  );
}

const Stack = createNativeStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="SignIn" component={SignIn}></Stack.Screen>
      <Stack.Screen name="Register" component={Register}></Stack.Screen>
      <Stack.Screen name="Forgot" component={Forgot}></Stack.Screen>
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#f4f4f4',
  },
  logoappbox: {},
  logoapp: {
    alignSelf: 'center',
    marginBottom: 10,
    marginTop: 40,
    height: 60,
    width: 260,
  },
  signintext: {
    fontFamily: 'Nunito',
    textAlign: 'center',
    fontSize: 20,
    marginTop: 8,
    marginBottom: 20,
  },
  inputbox: {},
  input: {
    borderWidth: 0,
    paddingLeft: 15,
    width: 350,
    alignSelf: 'center',
    marginTop: 10,
    marginBottom: 10,
    borderRadius: 3,
    backgroundColor: 'white',
  },

  buttonbox: {},
  button: {
    alignItems: 'center',
    backgroundColor: '#2e3283',
    paddingTop: 15,
    paddingBottom: 15,
    marginTop: 10,
    marginBottom: 10,
    borderRadius: 5,
    width: 350,
    alignSelf: 'center',
  },
  buttontext: {
    color: 'white',
  },
  forpasstext: {
    color: 'darkblue',
    fontSize: 12,
    textAlign: 'center',
    marginTop: 10,
  },
  loginbox: {
    flexDirection: 'row',
    marginTop: 30,
  },
  garis: {
    flex: 1,
    height: 1,
    backgroundColor: 'grey',
    alignSelf: 'center',
    marginLeft: 20,
    marginRight: 20,
  },
  logintext: {
    fontSize: 12,
    color: 'darkblue',
  },
  imagebox: {
    flexDirection: 'row',
    paddingTop: 25,
    paddingBottom: 20,
    justifyContent: 'center',
  },
  logofb: {
    width: 50,
    height: 50,
    marginRight: 30,
  },
  logogoogle: {
    width: 50,
    height: 50,
    marginLeft: 30,
  },
  versionbox: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 5,
    marginBottom: 10,
  },
  versiontext1: {
    marginRight: 25,
  },
  versiontext2: {
    marginLeft: 25,
  },
  footerbox: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: 'white',
    paddingBottom: 10,
    paddingTop: 10,
    width: '100%',
    alignItems: 'center',
    shadowOffset: {width: 0, height: 12},
    shadowColor: '#000000',
    shadowOpacity: 1,
    shadowRadius: 16,
    elevation: 24,
  },
  footertext1: {},
  footertext2: {
    fontWeight: 'bold',
    fontSize: 13,
    textAlign: 'center',
  },
});
export default MyStack;
